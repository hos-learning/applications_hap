| HAP | permanent archive addresses |
| - | - |
| Calc_Demo.hap | https://cidownload.openharmony.cn/version/Master_Version/hap_Calc_Demo_test_with_sdk/20240329_095429/version-Master_Version-hap_Calc_Demo_test_with_sdk-20240329_095429-hap_Calc_Demo_test_with_sdk.tar.gz |
| CallUI.hap | http://download.ci.openharmony.cn/version/Master_Version/hap_CallUI_with_sdk/20230926_121610/version-Master_Version-hap_CallUI_with_sdk-20230926_121610-hap_CallUI_with_sdk.tar.gz |
| Camera.hap | https://cidownload.openharmony.cn/version/Master_Version/hap_Camera_with_sdk/20240409_144519/version-Master_Version-hap_Camera_with_sdk-20240409_144519-hap_Camera_with_sdk.tar.gz |
| CertificateManager.hap | https://cidownload.openharmony.cn/version/Master_Version/hap_CertificateManager_with_sdk/20240414_105728/version-Master_Version-hap_CertificateManager_with_sdk-20240414_105728-hap_CertificateManager_with_sdk.tar.gz |
| Clock_Demo.hap | https://cidownload.openharmony.cn/version/Master_Version/hap_Clock_Demo_test_with_sdk/20240314_113405/version-Master_Version-hap_Clock_Demo_test_with_sdk-20240314_113405-hap_Clock_Demo_test_with_sdk.tar.gz |
| Contacts.hap | http://download.ci.openharmony.cn/version/Master_Version/hap_Contacts_with_sdk/20240115_141251/version-Master_Version-hap_Contacts_with_sdk-20240115_141251-hap_Contacts_with_sdk.tar.gz |
| FilePicker.hap | http://download.ci.openharmony.cn/version/Master_Version/hap_FilePicker_with_sdk/20231216_172831/version-Master_Version-hap_FilePicker_with_sdk-20231216_172831-hap_FilePicker_with_sdk.tar.gz |
| Launcher.hap | http://download.ci.openharmony.cn/version/Master_Version/hap_Launcher_with_sdk/20240319_192448/version-Master_Version-hap_Launcher_with_sdk-20240319_192448-hap_Launcher_with_sdk.tar.gz|
| Launcher_Settings.hap | http://download.ci.openharmony.cn/version/Master_Version/hap_Launcher_with_sdk/20240319_192448/version-Master_Version-hap_Launcher_with_sdk-20240319_192448-hap_Launcher_with_sdk.tar.gz|
| Mms.hap | https://cidownload.openharmony.cn/version/Master_Version/hap_Mms_with_sdk/20231127_151057/version-Master_Version-hap_Mms_with_sdk-20231127_151057-hap_Mms_with_sdk.tar.gz |
| MobileDataSettings.hap | http://download.ci.openharmony.cn/version/Master_Version/hap_CallUI_with_sdk/20230926_121610/version-Master_Version-hap_CallUI_with_sdk-20230926_121610-hap_CallUI_with_sdk.tar.gz |
| Music_Demo.hap | https://cidownload.openharmony.cn/version/Master_Version/hap_Music_Demo_test_with_sdk/20240402_091935/version-Master_Version-hap_Music_Demo_test_with_sdk-20240402_091935-hap_Music_Demo_test_with_sdk.tar.gz |
| Note.hap | https://cidownload.openharmony.cn/version/Master_Version/hap_Note_with_sdk/20240314_160114/version-Master_Version-hap_Note_with_sdk-20240314_160114-hap_Note_with_sdk.tar.gz |
| Music_Demo.hap | http://download.ci.openharmony.cn/version/Master_Version/hap_Music_Demo_test_with_sdk/20231220_180430/version-Master_Version-hap_Music_Demo_test_with_sdk-20231220_180430-hap_Music_Demo_test_with_sdk.tar.gz |
| Note.hap | https://cidownload.openharmony.cn/version/Master_Version/hap_Note_with_sdk/20240328_141508/version-Master_Version-hap_Note_with_sdk-20240328_141508-hap_Note_with_sdk.tar.gz |
| Photos.hap | https://cidownload.openharmony.cn/version/Daily_Version/hap_Photos_with_sdk/20240402_112011/version-Daily_Version-hap_Photos_with_sdk-20240402_112011-hap_Photos_with_sdk.tar.gz |
| ScreenShot.hap | http://download.ci.openharmony.cn/version/Master_Version/hap_ScreenShot/20221124_163242/version-Master_Version-hap_ScreenShot-20221124_163242-hap_ScreenShot.tar.gz |
| Settings.hap | https://cidownload.openharmony.cn/version/Master_Version/hap_Settings_with_sdk/20240123_183834/version-Master_Version-hap_Settings_with_sdk-20240123_183834-hap_Settings_with_sdk.tar.gz |
| SettingsData.hap | http://download.ci.openharmony.cn/version/Master_Version/hap_SettingsData_with_sdk/20231016_172558/version-Master_Version-hap_SettingsData_with_sdk-20231016_172558-hap_SettingsData_with_sdk.tar.gz |
| Settings_FaceAuth.hap | http://download.ci.openharmony.cn/Artifacts/hap_build/20230424-1-00011/version/Artifacts-hap_build-20230424-1-00011-version-hap_build.tar.gz |
| SystemUI-DropdownPanel.hap | https://cidownload.openharmony.cn/version/Master_Version/hap_SystemUI_with_sdk/20240326_110736/version-Master_Version-hap_SystemUI_with_sdk-20240326_110736-hap_SystemUI_with_sdk.tar.gz |
| SystemUI-NavigationBar.hap | https://cidownload.openharmony.cn/version/Master_Version/hap_SystemUI_with_sdk/20240326_110736/version-Master_Version-hap_SystemUI_with_sdk-20240326_110736-hap_SystemUI_with_sdk.tar.gz |
| SystemUI-NotificationManagement.hap | https://cidownload.openharmony.cn/version/Master_Version/hap_SystemUI_with_sdk/20240326_110736/version-Master_Version-hap_SystemUI_with_sdk-20240326_110736-hap_SystemUI_with_sdk.tar.gz |
| SystemUI-ScreenLock.hap | https://cidownload.openharmony.cn/version/Master_Version/hap_ScreenLock_with_sdk/20240326_163549/version-Master_Version-hap_ScreenLock_with_sdk-20240326_163549-hap_ScreenLock_with_sdk.tar.gz |
| SystemUI-StatusBar.hap | https://cidownload.openharmony.cn/version/Master_Version/hap_SystemUI_with_sdk/20240326_110736/version-Master_Version-hap_SystemUI_with_sdk-20240326_110736-hap_SystemUI_with_sdk.tar.gz |
| SystemUI-VolumePanel.hap | https://cidownload.openharmony.cn/version/Master_Version/hap_SystemUI_with_sdk/20240326_110736/version-Master_Version-hap_SystemUI_with_sdk-20240326_110736-hap_SystemUI_with_sdk.tar.gz |
| SystemUI.hap | https://cidownload.openharmony.cn/version/Master_Version/hap_SystemUI_with_sdk/20240326_110736/version-Master_Version-hap_SystemUI_with_sdk-20240326_110736-hap_SystemUI_with_sdk.tar.gz |
| SystemUI-SystemDialog.hap | https://cidownload.openharmony.cn/version/Master_Version/hap_SystemUI_with_sdk/20240326_110736/version-Master_Version-hap_SystemUI_with_sdk-20240326_110736-hap_SystemUI_with_sdk.tar.gz |
| kikaInput.hap | http://download.ci.openharmony.cn/version/Master_Version/hap_kikaInput_with_sdk/20230926_102115/version-Master_Version-hap_kikaInput_with_sdk-20230926_102115-hap_kikaInput_with_sdk.tar.gz |
| UpdateApp.hap | http://download.ci.openharmony.cn/version/Daily_Version/hap_UpdateApp_with_sdk/20230605_003549/version-Daily_Version-hap_UpdateApp_with_sdk-20230605_003549-hap_UpdateApp_with_sdk.tar.gz |
| PrintSpooler.hap | http://download.ci.openharmony.cn/version/Master_Version/hap_test1/20240103_115149/version-Master_Version-hap_test1-20240103_115149-hap_print_spooler_with_sdk.tar.gz |
| CalendarData.hap | http://download.ci.openharmony.cn/version/Master_Version/hap_calendar_calendardata_with_sdk/20231206_171639/version-Master_Version-hap_calendar_calendardata_with_sdk-20231206_171639-hap_calendar_calendardata_with_sdk.tar.gz |
| SecurityPrivacyCenter.hap | https://cidownload.openharmony.cn/version/Master_Version/hap_CertificateManager_with_sdk/20240414_105728/version-Master_Version-hap_CertificateManager_with_sdk-20240414_105728-hap_CertificateManager_with_sdk.tar.gz |

| SDK | optional download urls |
| - | - |
| 3.1.7.7 | https://mirrors.huaweicloud.com/openharmony/os/3.1.2/sdk-patch/ohos-sdk-full.tar.gz |
| 3.2.7.5 | https://repo.huaweicloud.com/harmonyos/os/3.2-Beta3/ohos-sdk-windows_linux-full.tar.gz |
| 3.2.7.6 | https://repo.huaweicloud.com/harmonyos/os/3.2-Beta3/sdk-patch/ohos-sdk-full.tar.gz |
| 3.2.8.3 | http://download.ci.openharmony.cn/version/Master_Version/OpenHarmony_3.2.8.3/20221031_100640/version-Master_Version-OpenHarmony_3.2.8.3-20221031_100640-ohos-sdk-full.tar.gz |
